// Fill out your copyright notice in the Description page of Project Settings.


#include "TPS_IGameActor.h"

// Add default functionality here for any ITPS_IGameActor functions that are not pure virtual.
EPhysicalSurface ITPS_IGameActor::GetSurfaceType()
{
	return EPhysicalSurface::SurfaceType_Default;
}

TArray<UTPSStateEffect*> ITPS_IGameActor::GetAllCurrentEffects()
{
	TArray<UTPSStateEffect*> Effect;
	return Effect;
}

void ITPS_IGameActor::RemoveEffect(UTPSStateEffect* RemoveEffect)
{
	
}

void ITPS_IGameActor::AddEffect(UTPSStateEffect* newEffect)
{
	
}

FVector ITPS_IGameActor::GetEffectLocation(UTPSStateEffect* Effect)
{
	return FVector(0);
}

FString ITPS_IGameActor::GetAttachComponent(UTPSStateEffect* Effect)
{
	return Effect->myActor->GetRootComponent()->GetName();
}

