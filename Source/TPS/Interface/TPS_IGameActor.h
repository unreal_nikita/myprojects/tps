// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "TPS/FuncLibraty/TPSStateEffect.h"
#include "TPS/FuncLibraty/Types.h"
#include "TPS_IGameActor.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UTPS_IGameActor : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class TPS_API ITPS_IGameActor
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	virtual EPhysicalSurface GetSurfaceType();

	virtual TArray<UTPSStateEffect*> GetAllCurrentEffects();
	virtual void RemoveEffect(UTPSStateEffect* RemoveEffect);
	virtual void AddEffect(UTPSStateEffect* newEffect);
	virtual FVector GetEffectLocation(UTPSStateEffect* Effect);
	virtual FString GetAttachComponent(UTPSStateEffect* Effect);

	//inventory
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void DropWeaponToWorld(FDropItem DropItemInfo);
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void DropAmmoToWorld(EWeaponType TypeAmmo, int32 Cout);
};
