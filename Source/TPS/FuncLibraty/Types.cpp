// Fill out your copyright notice in the Description page of Project Settings.


#include "Types.h"
#include "TPS/Interface/TPS_IGameActor.h"

void UTypes::AddEffectBySurfaceType(AActor* TakeEffectActor, TSubclassOf<UTPSStateEffect> AddEffectClass,
	EPhysicalSurface SurfaceType, FName NameBoneHit)
{
	if (SurfaceType != EPhysicalSurface::SurfaceType_Default && TakeEffectActor && AddEffectClass)
	{
		UTPSStateEffect* myEffect = Cast<UTPSStateEffect>(AddEffectClass->GetDefaultObject());
		if (myEffect)
		{
			bool bIsHavePossibleSurface = false;
			int8 i = 0;
			while (i < myEffect->PossibleInteractSurface.Num() && !bIsHavePossibleSurface)
			{
				if (myEffect->PossibleInteractSurface[i] == SurfaceType)
				{			
					bIsHavePossibleSurface = true;
					bool bIsCanAddEffect = false;
					if (!myEffect->bIsStakable)
					{
						int8 j = 0;
						TArray<UTPSStateEffect*> CurrentEffects;
						ITPS_IGameActor* myInterface = Cast<ITPS_IGameActor>(TakeEffectActor);
						if (myInterface)
						{
							CurrentEffects = myInterface->GetAllCurrentEffects();
						}

						if (CurrentEffects.Num() > 0)
						{
							while (j < CurrentEffects.Num() && !bIsCanAddEffect)
							{
								if (CurrentEffects[j]->GetClass() != AddEffectClass)
								{
									bIsCanAddEffect = true;
								}
								j++;
							}
						}
						else
						{
							bIsCanAddEffect = true;
						}
						
					}
					else
					{
						bIsCanAddEffect = true;
					}

					if (bIsCanAddEffect)
					{
						
						UTPSStateEffect* NewEffect = NewObject<UTPSStateEffect>(TakeEffectActor, AddEffectClass);
						if (NewEffect)
						{
							NewEffect->InitObject(TakeEffectActor, NameBoneHit);
						}
					}
				}
				i++;
			}
		}

	}
}
