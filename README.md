# TPS

Developed with Unreal Engine 4

## Description

Made Top Down Shooter (TPS) for practice

![Screenshot](Images/Screenshot.png)

## Last Update

Made AI and AI Boss and changed rules for boss and spawn settings

## Plans

1. Multiplayer
    1.1 Conecting host
    1.2 Changed some Actor replication
2. Polishing
3. Refactoring

## Project table

https://trello.com/invite/b/WzFzJBAK/b6fe52f070644a688fa46ec7f1fc0ff8/tps
